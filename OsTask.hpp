#ifndef OSTASK_HPP
#define OSTASK_HPP

#include "OsStatus.hpp" // For OsStatus
#include <stdint.h>

namespace Osal
{
// Task function type
typedef void ( *OsTaskFunction )( void* );
}

#include "OsTaskImpl.hpp" // For system specific declarations.

namespace Osal
{
Status OsTaskCreate( OsTaskFunction taskExecutionFunction, void* pParameters,
                     uint16_t stackSize, uint8_t priority,
                     const char* pTaskname, OsTaskType& pTaskHandle );

Status OsTaskDelete( OsTaskType& taskToDelete );
Status OsTaskResume( OsTaskType& taskToResume );
Status OsTaskSuspend( OsTaskType& taskToSuspend );

Status OsTaskDelayMs( const uint32_t delayMs );
Status OsTaskDelayUs( const uint32_t delayUs );
}
#endif // OSTASK_HPP
