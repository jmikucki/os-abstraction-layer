#include "OsAssert.hpp"
#include "OsStatus.hpp"

namespace Osal
{
Status OsInitialize()
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsStart()
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}
}
