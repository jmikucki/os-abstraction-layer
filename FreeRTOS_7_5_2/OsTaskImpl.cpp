#include "OsAssert.hpp"
#include "OsStatus.hpp"
#include "OsTask.hpp"
#include <cstdint>
#include <cstring>
#include <new>
#include <unistd.h> // For usleep

#include "task.h"

namespace Osal
{
void* OsTaskFunctionWrapper( void* pParameter )
{
    OsTaskWrapperParameters* pWrappedParameters =
      reinterpret_cast<OsTaskWrapperParameters*>( pParameter );

    OsTaskWrapperParameters parameters;
    std::memcpy( &parameters, pWrappedParameters,
                 sizeof( OsTaskWrapperParameters ) );

    delete pWrappedParameters;

    parameters.pFunction( parameters.pParameters );

    return 0;
}

Status OsTaskCreate( OsTaskFunction taskExecutionFunction, void* pParameters,
                     uint16_t stackSize, uint8_t priority,
                     const char* pTaskname, OsTaskType& pTaskHandle )
{
    size_t stackAlignment = ( stackSize % sizeof( StackType_t ) );

    if ( xTaskCreate( taskExecutionFunction,
                      reinterpret_cast<OsTaskNameType>( pTaskname ),
                      ( stackSize + sizeof( StackType_t ) - stackAlignment ) /
                        sizeof( StackType_t ),
                      pParameters, priority, &pTaskHandle ) == pdPASS )
    {
        return Status::OK;
    }
    else
    {
        return Status::ERROR;
    }
}

Status OsTaskDelete( OsTaskType& taskToDelete )
{
    vTaskDelete( taskToDelete );
    return Status::OK;
}

Status OsTaskResume( OsTaskType& taskToResume )
{
    vTaskResume( taskToResume );
    return Status::OK;
}

Status OsTaskSuspend( OsTaskType& taskToSuspend )
{
    vTaskSuspend( taskToSuspend );
    return Status::OK;
}

Status OsTaskDelayMs( const uint32_t delayMs )
{
    vTaskDelay( delayMs / portTICK_RATE_MS );
    return Status::OK;
}

Status OsTaskDelayUs( const uint32_t delayUs )
{
    OsAssert( delayUs < UINT32_MAX / 1000 );
    return OsTaskDelayMs( delayUs * 1000 );
}
}
