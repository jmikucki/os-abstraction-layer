#ifndef FREERTOS_OSTASKIMPL_HPP
#define FREERTOS_OSTASKIMPL_HPP

#include "FreeRTOS.h"
#include "task.h"
#include <cstdint>

typedef xTaskHandle TaskHandle_t;   // For FreeRTOS 8+ compatibiltity
typedef portSTACK_TYPE StackType_t; // For FreeRTOS 8+ compatibiltity

namespace Osal
{
// Types
typedef TaskHandle_t OsTaskType;
typedef const signed char* OsTaskNameType;

struct OsTaskWrapperParameters
{
    OsTaskFunction pFunction;
    void* pParameters;
};

void* OsTaskFunctionWrapper( void* pParameter );
}
#endif // FREERTOS_OSTASKIMPL_HPP
