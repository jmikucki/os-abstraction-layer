#ifndef FREERTOS_OSINTERPROCESSIMPL_HPP
#define FREERTOS_OSINTERPROCESSIMPL_HPP

#include "FreeRTOS.h"

typedef portBASE_TYPE BaseType_t; // For FreeRTOS 8+ compatibiltity

#include "OsStatus.hpp"
#include "queue.h"  // For FreeRtos queue
#include "semphr.h" // For FreeRtos Mutex and semaphore
#include <cstdint>

namespace Osal
{
typedef xSemaphoreHandle OsMutexType;
typedef xSemaphoreHandle OsSemaphoreType;
typedef xQueueHandle OsQueueType;
}
#endif // FREERTOS_OSINTERPROCESSIMPL_HPP
