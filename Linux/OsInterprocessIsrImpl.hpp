#ifndef LINUX_OSINTERPROCESSISRIMPL_HPP
#define LINUX_OSINTERPROCESSISRIMPL_HPP

#include <cstdint>
#include <semaphore.h>

namespace Osal
{
typedef ::sem_t OsSemaphoreType;
typedef OsQueueStruct OsQueueType;
}
#endif // LINUX_OSINTERPROCESSISRIMPL_HPP
