#include "OsAssert.hpp"
#include "OsSocket.hpp"

#include <cstdint> // For uint16_t, uint32_t
#include <cstring> // For size_t
#include <unistd.h>

#include <errno.h>
#include <netinet/in.h>

namespace Osal
{
SocketStatus OsSocketCreate( OsSocketType& socket, SocketProtocol protocol,
                             SocketBlocking blocking )
{
    OsAssert( protocol == SocketProtocol::TCP );
    OsAssert( blocking == SocketBlocking::YES );

    socket.protocol = protocol;
    socket.blocking = blocking;

    socket.fdSocket = ::socket( AF_INET, SOCK_STREAM, 0 );
    return SocketStatus::OK;
}

SocketStatus OsSocketListen( OsSocketType& socket, const uint32_t ipAddress,
                             const uint16_t port )
{
    struct sockaddr_in serv_addr;
    std::memset( &serv_addr, sizeof( serv_addr ), 0x00 );

    serv_addr.sin_family      = AF_INET;
    serv_addr.sin_port        = htons( port );
    serv_addr.sin_addr.s_addr = INADDR_ANY;

    // Bind to any address
    if ( bind( socket.fdSocket, (struct sockaddr*)&serv_addr,
               sizeof( serv_addr ) ) < 0 )
    {
        return SocketStatus::ERROR;
    }

    // Listen with default backlog.
    if ( listen( socket.fdSocket, OSSOCKET_BACKLOG_SIZE ) < 0 )
    {
        return SocketStatus::ERROR;
    }

    return SocketStatus::OK;
}

SocketStatus OsSocketAccept( OsSocketType& listenSocket,
                             OsSocketType& acceptedSocket )
{
    OsSocketType socket;
    if ( ( socket.fdSocket = accept( listenSocket.fdSocket, NULL, NULL ) ) < 0 )
    {
        return SocketStatus::ERROR;
    }

    // Success, store socket.
    acceptedSocket = socket;
    return SocketStatus::OK;
}

SocketStatus OsSocketConnect( OsSocketType& socket, const uint32_t ipAddress,
                              const uint16_t port )
{
    OsAssert( false );
    return SocketStatus::NOTIMPLEMENTED;
}

SocketStatus OsSocketClose( const OsSocketType& socket )
{
    close( socket.fdSocket );
    return SocketStatus::OK;
}

SocketStatus OsSocketSend( const OsSocketType& socket,
                           const uint8_t* const data, size_t& length )
{
    ssize_t sendResult = 0;

    while ( true )
    {
        if ( ( sendResult = send( socket.fdSocket, data, length, 0 ) ) < 0 )
        {
            if ( errno == EINTR )
            {
                continue;
            }
            return SocketStatus::ERROR;
        }
        else if ( sendResult == 0 )
        {
            return SocketStatus::CLOSED;
        }

        length = sendResult;
        return SocketStatus::OK;
    }
}

SocketStatus OsSocketReceive( const OsSocketType& socket, uint8_t* const data,
                              size_t& length )
{
    ssize_t recvResult = 0;

    while ( true )
    {

        if ( ( recvResult = recv( socket.fdSocket, data, length, 0 ) ) < 0 )
        {
            if ( errno == EINTR )
            {
                continue;
            }
            return SocketStatus::ERROR;
        }
        else if ( recvResult == 0 )
        {
            return SocketStatus::CLOSED;
        }

        length = recvResult;
        return SocketStatus::OK;
    }
}
}
