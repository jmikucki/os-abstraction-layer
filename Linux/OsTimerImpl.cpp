#include "OsAssert.hpp"
#include "OsStatus.hpp" // For OsStatus
#include "OsTimer.hpp"  // For system specific declarations.

namespace Osal
{
Status OsTimerCreate( OsTimer& pTimer )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsTimerStart( OsTimer& pTimer )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsTimerStop( OsTimer& pTimer )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}
}
