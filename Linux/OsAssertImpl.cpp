#include <iostream>

namespace Osal
{
void OsAssertImpl( bool condition, const char* conditionStr,
                   const char* fileName, const int lineNumber )
{
    if ( !condition )
    {
        std::cout << "Assert in " << fileName << " line " << std::dec
                  << lineNumber << std::endl;
        std::cout << "Parameter evaluated to false: " << conditionStr
                  << std::endl;
        exit( 0 );
    }
}
}
