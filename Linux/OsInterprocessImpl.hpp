#ifndef LINUX_OSINTERPROCESSIMPL_HPP
#define LINUX_OSINTERPROCESSIMPL_HPP

#include "OsStatus.hpp"

#include <cstdint>
#include <pthread.h>
#include <semaphore.h>

namespace Osal
{
struct OsQueueStruct
{
    pthread_mutex_t queueLock;

    // Empty semaphore is given once for each of empty message possitions.
    sem_t notificationSemaphoreEmpty;
    // Full semaphore is given once for each of used message possitions.
    sem_t notificationSemaphoreFull;
    uint32_t threadsWaitingOnSemaphore;

    bool isQueueInitialized;
    size_t numberOfElementsMax;
    size_t numberOfElementsCurrent;
    size_t sizeOfElement;
    uint8_t* pElementsArray;

    // Queue is realized as a fifo.
    size_t firstEmpty;
    size_t firstFull;
};

typedef ::pthread_mutex_t OsMutexType;
typedef ::sem_t OsSemaphoreType;
typedef OsQueueStruct OsQueueType;
}
#endif // LINUX_OSINTERPROCESSIMPL_HPP
