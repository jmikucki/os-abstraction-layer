#include "OsAssert.hpp"
#include "OsStatus.hpp"
#include "OsTask.hpp"
#include <cstdint>
#include <cstring>
#include <new>
#include <unistd.h> // For usleep

namespace Osal
{
void* OsTaskFunctionWrapper( void* pParameter )
{
    OsTaskWrapperParameters* pWrappedParameters =
      reinterpret_cast<OsTaskWrapperParameters*>( pParameter );

    OsTaskWrapperParameters parameters;
    std::memcpy( &parameters, pWrappedParameters,
                 sizeof( OsTaskWrapperParameters ) );

    delete pWrappedParameters;

    parameters.pFunction( parameters.pParameters );

    return 0;
}

Status OsTaskCreate( OsTaskFunction taskExecutionFunction, void* pParameters,
                     uint16_t stackSize, uint8_t priority,
                     const char* pTaskname, OsTaskType& pTaskHandle )
{
    Status result = Status::ERROR;

    // Thread attributes.
    pthread_attr_t threadAttributes;
    OsAssert( pthread_attr_init( &threadAttributes ) == 0 );
    OsAssert( pthread_attr_setstacksize(
                &threadAttributes, static_cast<size_t>( stackSize ) ) == 0 );

    // TODO: Add priority setting to the thread.
    // Policy should be FIFO
    // Priority should be set according to priority parameter
    // Linux doesn't support PTHREAD_SCOPE_PROCESS scheduling scope

    // Prepare wrapper parameters.
    OsTaskWrapperParameters* pWrapperParameters =
      new ( std::nothrow ) OsTaskWrapperParameters;
    OsAssert( pWrapperParameters != NULL );
    pWrapperParameters->pFunction   = taskExecutionFunction;
    pWrapperParameters->pParameters = pParameters;

    // Create thread. Use NULL as default thread attributes.
    if ( pthread_create( &pTaskHandle, &threadAttributes, OsTaskFunctionWrapper,
                         pWrapperParameters ) == 0 )
    {
        result = Status::OK;
    }

    OsAssert( pthread_attr_destroy( &threadAttributes ) == 0 );

    return result;
}

Status OsTaskDelete( OsTaskType& taskToDelete )
{
    // OsAssert( false );
    // return OsNOTIMPLEMENTED;

    // Pthread don't have a function to remove thread. It should finish on it's
    // own.
    return Status::OK;
}

Status OsTaskResume( OsTaskType& taskToResume )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsTaskSuspend( OsTaskType& taskToSuspend )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsTaskDelayMs( const uint32_t delayMs )
{
    return OsTaskDelayUs( delayMs * 1000 );
}

Status OsTaskDelayUs( const uint32_t delayUs )
{
    OsAssert( usleep( delayUs ) == 0 );

    return Status::OK;
}
}
