#ifndef LINUX_OSSOCKETIMPL_HPP
#define LINUX_OSSOCKETIMPL_HPP

#include "OsSocket.hpp"
#include <cstdint>
#include <cstring>
#include <sys/socket.h>
#include <sys/types.h>

namespace Osal
{
struct SocketStruct
{
    int fdSocket;
    SocketProtocol protocol;
    SocketBlocking blocking;
};

typedef SocketStruct OsSocketType;
}
#endif // LINUX_OSSOCKETIMPL_HPP
