/// \file
/// Contains functions used to communicate between processes and for
/// synchronization. Functions in this file can be called in context of ISR. NOT
/// YET WELL DESIGNED.

#ifndef OSINTERPROCESSISR_HPP
#define OSINTERPROCESSISR_HPP

#include "OsInterprocessIsrImpl.hpp" // For system specific declarations.
#include "OsStatus.hpp"              // For OsStatus
#include <cstring>                   // For size_t

namespace Osal
{
/// \brief Takes (decrements, P) a semaphore. ISR safe.
///
/// Decrements the semahore. a ISR calling this function isn't blocked, but the
/// function returns error value denoting that it would block.
/// \param[out] rSemaphore      Semaphore to be taken.
/// \retval     Status::OK    Took semaphore successfully.
/// \retval     Status::WOULDBLOCK Can't take semaphore at this time.
/// \retval     Status::ERROR Error. Semaphore is in unknown state.
Status OsSemaphoreNonBlockingTakeIsr( OsSemaphoreType& rSemaphore );

/// \brief Gives (increments, V) a semaphore. ISR safe.
///
/// Incremnts the semahore, a ISR calling this function isn't blocked, but the
/// function returns error value denoting that it would block.
/// \param[out] rSemaphore      Semaphore to be given.
/// \retval     Status::OK    Gave semaphore successfully.
/// \retval     Status::WOULDBLOCK Can't give semaphore at this time.
/// \retval     Status::ERROR Error. Semaphore is in unknown state.
Status OsSemaphoreNonBlockingGiveIsr( OsSemaphoreType& rSemaphore );

/// \brief Receives an item from a queue. Doesn't block if data is not
/// available. Can be called from ISR.
///
/// If data is not available in the queue, ISR calling this function isn't
/// blocked, but the function returns error value denoting that it would block.
/// Should be called again when data becomes available.
/// \param[out] rQueue          Queue to be created.
/// \param[in]  pItemBuffer     Pointer to buffer for received item.
/// \retval     Status::OK            Received data successfully.
/// \retval     Status::WOULDBLOCK    No data available to receive.
/// \retval     Status::ERROR         Error. Queue in unknown state.
Status OsQueueReceiveNonBlockingIsr( OsQueueType& rQueue,
                                     void* const pItemBuffer );

/// \brief Sends an item to a queue. Doesn't block if queue is full. Can be
/// called from ISR.
///
/// If queue is full, ISR calling this function isn't blocked , but the function
/// returns error value denoting that it would block. Should be called
/// again when buffer space in the queue becomes avaiable.
/// \param[out] rQueue          Queue to be created.
/// \param[in]  pItemToSend     Pointer to item to be sent.
/// \retval     Status::OK    Received data successfully.
/// \retval     Status::WOULDBLOCK    No data available to receive.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueNonBlockingSendIsr( OsQueueType& rQueue,
                                  const void* const pItemToSend );

/// \brief Checks if queue is empty. ISR safe.
///
/// Checks in threadsafe manner if the queue is empty.
/// \param[out] rQueue          Queue to check.
/// \retval     Status::TRUE  Queue is empty.
/// \retval     Status::FALSE Queue is not empty.
/// \retval     Status::WOULDBLOCK Can't check queue at this time.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueIsEmptyIsr( const OsQueueType& rQueue );

/// \brief Checks if queue if full. ISR safe.
///
/// Checks in threadsafe manner if the queue is full.
/// \param[out] rQueue          Queue to check.
/// \retval     Status::TRUE  Queue is full.
/// \retval     Status::FALSE Queue is not full.
/// \retval     Status::WOULDBLOCK Can't check queue at this time.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueIsFullIsr( const OsQueueType& rQueue );
}
#endif // OSINTERPROCESSISR_HPP
