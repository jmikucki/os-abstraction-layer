// ----------------------------------------------------------------------------
//  Endianess.hpp
// ----------------------------------------------------------------------------
//
//  Description:
//      This file contains the inlines to store/read data of given endianness.
//
//  License:
//      All rights reserved 2013.
//
//  File parameters:
//      Indentations: 4 spaces
//      Coding: UTF-8
//
//
//
//
// ----------------------------------------------------------------------------

#ifndef ENDIANESS_HPP
#define ENDIANESS_HPP

// ----------------------------------------------------------------------------
//  Includes
#include <cstdint>
// ----------------------------------------------------------------------------

// inline uint32_t HostLEu32( uint8_t* dataIn )
// {
//     return ( ( *( dataIn + 3 ) << 24 ) & 0xFF000000 ) |
//            ( ( *( dataIn + 2 ) << 16 ) & 0x00FF0000 ) |
//            ( ( *( dataIn + 1 ) << 8 ) & 0x0000FF00 ) |
//            ( ( *( dataIn + 0 ) << 0 ) & 0x000000FF );
// }
//
// inline uint32_t HostLEu32( uint32_t& dataIn )
// {
//     return HostLEu32( reinterpret_cast<uint8_t*>( &dataIn ) );
// }

inline uint16_t HostLEu16( uint8_t* dataIn )
{
    return ( ( *( dataIn + 1 ) << 8 ) & 0x0000FF00 ) | ( *dataIn & 0x000000FF );
}

inline uint16_t HostLEu16( uint16_t& dataIn )
{
    return HostLEu16( reinterpret_cast<uint8_t*>( &dataIn ) );
}

inline uint32_t HostBEu32( uint8_t* dataIn )
{
    return ( ( *( dataIn + 0 ) << 24 ) & 0xFF000000 ) |
           ( ( *( dataIn + 1 ) << 16 ) & 0x00FF0000 ) |
           ( ( *( dataIn + 2 ) << 8 ) & 0x0000FF00 ) |
           ( ( *( dataIn + 3 ) << 0 ) & 0x000000FF );
}

inline uint32_t HostBEu32( uint32_t& dataIn )
{
    return HostBEu32( reinterpret_cast<uint8_t*>( &dataIn ) );
}

inline uint16_t HostBEu16( uint8_t* dataIn )
{
    return ( ( *dataIn << 8 ) & 0x0000FF00 ) | ( *( dataIn + 1 ) & 0x000000FF );
}

inline uint16_t HostBEu16( uint16_t& dataIn )
{
    return HostBEu16( reinterpret_cast<uint8_t*>( &dataIn ) );
}

inline uint32_t HostNetworku32( uint8_t* dataIn )
{
    return ( ( *( dataIn + 0 ) << 0 ) & 0x000000FF ) |
           ( ( *( dataIn + 1 ) << 8 ) & 0x0000FF00 ) |
           ( ( *( dataIn + 2 ) << 16 ) & 0x00FF0000 ) |
           ( ( *( dataIn + 3 ) << 24 ) & 0xFF000000 );
}

inline uint32_t HostNetworku32( uint32_t& dataIn )
{
    return dataIn;
    // return HostBEu32(reinterpret_cast<uint8_t*>(&dataIn));
}

inline uint16_t HostNetworku16( uint8_t* dataIn )
{
    return ( ( *( dataIn + 1 ) << 8 ) & 0x0000FF00 ) | ( *(dataIn)&0x000000FF );
}

inline uint16_t HostNetworku16( uint16_t& dataIn )
{
    return dataIn;

    // return HostBEu16(reinterpret_cast<uint8_t*>(&dataIn));
}

#endif // ENDIANESS_HPP
