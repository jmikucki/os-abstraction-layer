#ifndef OSASSERT_HPP
#define OSASSERT_HPP

#include "OsAssertImpl.hpp" // For system specific declarations.

#define OsAssert( x ) OsAssertImpl( x, #x, __FILE__, __LINE__ )

#ifdef DEBUG
#define OsAssertDbg( x ) OsAssertImpl( x, #x, __FILE__, __LINE__ )
#else
#define OsAssertDbg( x ) static_cast<void>( x )
#endif

namespace Osal
{
void OsAssertImpl( bool condition, const char* conditionStr,
                   const char* fileName, const int lineNumber );
}
#endif // OSASSERT_HPP
