#ifndef FREERTOS_OSINTERPROCESSIMPL_HPP
#define FREERTOS_OSINTERPROCESSIMPL_HPP

#include "FreeRTOS.h"

#include "OsStatus.hpp"
#include "queue.h"  // For FreeRtos queue
#include "semphr.h" // For FreeRtos Mutex and semaphore
#include <cstdint>

namespace Osal
{
typedef SemaphoreHandle_t OsMutexType;
typedef SemaphoreHandle_t OsSemaphoreType;
typedef QueueHandle_t OsQueueType;
}
#endif // FREERTOS_OSINTERPROCESSIMPL_HPP
