#ifndef FREERTOS_OSTIMEIMPL_HPP
#define FREERTOS_OSTIMEIMPL_HPP

#include "FreeRTOS.h" // For tick rate.
#include <stdint.h>

namespace Osal
{
// static const uint32_t configTICK_RATE_HZ   = 1000U;
static const uint32_t MICROSECONDS_IN_TICK = 1000000 / configTICK_RATE_HZ;
}

#endif // FREERTOS_OSTIMEIMPL_HPP
