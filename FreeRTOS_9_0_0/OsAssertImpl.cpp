#include <cstdio>

namespace Osal
{
void OsAssertImpl( bool condition, const char* conditionStr,
                   const char* fileName, const int lineNumber )
{
    if ( !condition )
    {
        printf( "Assert in %s line %d\n", fileName, lineNumber );
        printf( "Parameter evaluated to false: %s\n", conditionStr );
        while ( true )
        {
        }
    }
}
}
