#ifndef FREERTOS_OSINTERPROCESSISRIMPL_HPP
#define FREERTOS_OSINTERPROCESSISRIMPL_HPP

#include "FreeRTOS.h"

#include "queue.h"  // For FreeRtos queue
#include "semphr.h" // For FreeRtos Mutex and semaphore
#include <cstdint>

namespace Osal
{
typedef SemaphoreHandle_t OsSemaphoreType;
typedef QueueHandle_t OsQueueType;
}
#endif // FREERTOS_OSINTERPROCESSISRIMPL_HPP
