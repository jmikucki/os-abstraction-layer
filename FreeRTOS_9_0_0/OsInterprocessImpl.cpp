#include "OsAssert.hpp"
#include "OsInterprocess.hpp"
#include "OsStatus.hpp"
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <errno.h>

//------------------------------------------------------------------------------
// Mutex
//------------------------------------------------------------------------------
namespace Osal
{
// Create mutex. Return OsOK if success
Status OsMutexCreate( OsMutexType& rMutex )
{
    rMutex = xSemaphoreCreateMutex();
    if ( rMutex != NULL )
    {
        return Status::OK;
    }
    else
    {
        return Status::ERROR;
    }
}

Status OsMutexDelete( OsMutexType& rMutex )
{
    vSemaphoreDelete( rMutex );

    return Status::OK;
}

Status OsMutexLock( OsMutexType& rMutex )
{
    bool ret = xSemaphoreTake( rMutex, portMAX_DELAY );

    if ( ret == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }
}

Status OsMutexUnlock( OsMutexType& rMutex )
{
    bool ret = xSemaphoreGive( rMutex );

    if ( ret == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }
}

//------------------------------------------------------------------------------
// Semaphore
//------------------------------------------------------------------------------
Status OsSemaphoreCreate( OsSemaphoreType& rSemaphore )
{
    rSemaphore = xSemaphoreCreateBinary();
    if ( rSemaphore != NULL )
    {
        return Status::OK;
    }
    else
    {
        return Status::ERROR;
    }
}

Status OsSemaphoreDelete( OsSemaphoreType& rSemaphore )
{
    vSemaphoreDelete( rSemaphore );

    return Status::OK;
}

Status OsSemaphoreBlockingTake( OsSemaphoreType& rSemaphore )
{
    bool ret = xSemaphoreTake( rSemaphore, portMAX_DELAY );

    if ( ret == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }
}

Status OsSemaphoreNonBlockingTake( OsSemaphoreType& rSemaphore )
{
    bool ret = xSemaphoreTake( rSemaphore, 0 );

    if ( ret == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        return Status::WOULDBLOCK;
    }
}

Status OsSemaphoreNonBlockingGive( OsSemaphoreType& rSemaphore )
{
    bool ret = xSemaphoreGive( rSemaphore );

    if ( ret == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }
}

//------------------------------------------------------------------------------
// Critical Section
//------------------------------------------------------------------------------

Status OsCriticalSectionEnter()
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsCriticalSectionExit()
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

//------------------------------------------------------------------------------
// Queue
//------------------------------------------------------------------------------
Status OsQueueCreate( OsQueueType& rQueue, size_t queueLength,
                      size_t elementSize )
{
    rQueue = xQueueCreate( queueLength, elementSize );

    if ( rQueue != NULL )
    {
        return Status::OK;
    }
    else
    {
        return Status::ERROR;
    }
}

Status OsQueueDelete( OsQueueType& rQueue )
{
    vQueueDelete( rQueue );

    return Status::OK;
}

Status OsQueueReceiveNonBlocking( OsQueueType& rQueue, void* const pItemBuffer )
{
    if ( xQueueReceive( rQueue, pItemBuffer, 0 ) == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        return Status::WOULDBLOCK;
    }
}

Status OsQueueReceiveBlocking( OsQueueType& rQueue, void* const pItemBuffer )
{
    if ( xQueueReceive( rQueue, pItemBuffer, portMAX_DELAY ) == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }
}

Status OsQueueSendNonBlocking( OsQueueType& rQueue,
                               const void* const pItemToSend )
{
    if ( xQueueSendToBack( rQueue, pItemToSend, 0 ) == pdPASS )
    {
        return Status::OK;
    }
    else
    {
        return Status::WOULDBLOCK;
    }
}

Status OsQueueSendBlocking( OsQueueType& rQueue, const void* const pItemToSend )
{
    if ( xQueueSendToBack( rQueue, pItemToSend, portMAX_DELAY ) == pdPASS )
    {
        return Status::OK;
    }
    else
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }
}

Status OsQueueIsEmpty( const OsQueueType& rQueue )
{
    if ( uxQueueMessagesWaiting( rQueue ) == 0 )
    {
        return Status::TRUE;
    }
    else
    {
        return Status::FALSE;
    }
}

Status OsQueueIsFull( const OsQueueType& rQueue )
{
    if ( uxQueueSpacesAvailable( rQueue ) == 0 )
    {
        return Status::TRUE;
    }
    else
    {
        return Status::FALSE;
    }
}
}
