#include "OsAssert.hpp"
#include "OsStatus.hpp" // For OsStatus
#include "OsTime.hpp"   // For Ticks conversions.
#include "OsTimer.hpp"  // For system specific declarations.

namespace Osal
{

void OsTimerWrapper( TimerHandle_t pTimerArg )
{
    // Find sofware timer based on id.
    OsTimer* pTimer = (OsTimer*)pvTimerGetTimerID( pTimerArg );

    // Then use a callback with parameters.
    pTimer->pTimerCallback( pTimer->pTimerCallbackArgument );
}

Status OsTimerCreate( OsTimer& rTimer )
{
    // FreeRTOS implmentation based on software timer.
    OsAssert( rTimer.variant == OsTimerVariant::SOFTWARE );

    // If timer handle is not null, first delete.
    if ( rTimer.pTimer != NULL )
    {
        xTimerDelete( rTimer.pTimer, 0 );
        rTimer.pTimer = NULL;
    }

    // Create a new timer based on parameters. Use a wrapper as a callback
    // function, so parameters can be passed with that.
    rTimer.pTimer = xTimerCreate( "", OsMicrosecondsToTicks( rTimer.periodUs ),
                                  rTimer.periodic ? pdTRUE : pdFALSE,
                                  (void*)&rTimer, OsTimerWrapper );

    if ( rTimer.pTimer == NULL )
    {
        return Status::FALSE;
    }
    else
    {

        return Status::TRUE;
    }
}

Status OsTimerStart( OsTimer& rTimer )
{
    if ( xTimerStart( rTimer.pTimer, 0 ) == pdPASS )
    {
        return Status::TRUE;
    }
    else
    {
        return Status::FALSE;
    }
}

Status OsTimerStop( OsTimer& rTimer )
{
    xTimerStop( rTimer.pTimer, 0 );

    return Status::TRUE;
}
}
