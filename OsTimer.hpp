#ifndef OSTIMER_HPP
#define OSTIMER_HPP

#include "OsStatus.hpp"    // For OsStatus
#include "OsTimerImpl.hpp" // For system specific declarations.
#include <stdint.h>

namespace Osal
{
typedef void ( *OsTimerCallback )( void* );

enum class OsTimerVariant : uint8_t
{
    SOFTWARE,
    HARDWARE
};

struct OsTimer
{
    OsTimerType pTimer;
    OsTimerCallback pTimerCallback;
    void* pTimerCallbackArgument;
    OsTimerVariant variant;
    bool periodic;
    uint32_t periodUs;
};

Status OsTimerCreate( OsTimer& pTimer );
Status OsTimerDelete( OsTimer& pTimer );
Status OsTimerStart( OsTimer& pTimer );
Status OsTimerStop( OsTimer& pTimer );
}
#endif // OSTIMER_HPP
