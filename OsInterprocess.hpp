/// \file
/// Contains functions used to communicate between processes and for
/// synchronization. Functions in this file shouldn't be called in the context
/// of ISR.

#ifndef OSINTERPROCESS_HPP
#define OSINTERPROCESS_HPP

#include "OsInterprocessImpl.hpp" // For system specific declarations.
#include "OsStatus.hpp"           // For OsStatus
#include <cstring>                // For size_t

namespace Osal
{
/// \name OsMutex
///@{

/// \brief Creates a mutex.
///
/// Creates a mutex of OS specific type.
/// \param[out] rMutex      Mutex to be created.
/// \retval     Status::OK    Created the mutex successfully.
/// \retval     Status::ERROR Error. Mutex is in unknown state.
Status OsMutexCreate( OsMutexType& rMutex );

/// \brief Deletes a mutex.
///
/// Mutex to be deleted should be in unlocked state.
/// \param[out] rMutex      Mutex to be deleted.
/// \retval     Status::OK    Deleted the mutex successfully.
/// \retval     Status::ERROR Error. Mutex is in unknown state.
Status OsMutexDelete( OsMutexType& rMutex );

/// \brief Locks a mutex.
///
/// If a mutex is already locked, task calling the function is blocked until
/// mutex becomes available. Mutex can not be locked recursively.
/// \param[out] rMutex      Mutex to be locked.
/// \retval     Status::OK    Locked the mutex successfully.
/// \retval     Status::ERROR Error. Mutex is in unknown state.
Status OsMutexLock( OsMutexType& rMutex );

/// \brief Unlocks a mutex.
///
/// Mutex being unlocked should be in locked state. Mutex can be unlocked only
/// by the same task that locked it.
/// \param[out] rMutex      Mutex to be unlocked.
/// \retval     Status::OK    Unlocked the mutex successfully.
/// \retval     Status::ERROR Error. Mutex is in unknown state.
Status OsMutexUnlock( OsMutexType& rMutex );

///@}

/// \name OsSemaphore
///@{

/// \brief Creates a semaphore.
///
/// Creates a semaphore of OS specific type.
/// \param[out] rSemaphore      Semaphore to be created.
/// \retval     Status::OK    Created semaphore successfully.
/// \retval     Status::ERROR Error. Semaphore in unknown state.
Status OsSemaphoreCreate( OsSemaphoreType& rSemaphore );

/// \brief Deletes a semaphore.
///
/// \param[out] rSemaphore      Semaphore to be deleted.
/// \retval     Status::OK    Deleted semaphore successfully.
/// \retval     Status::ERROR Error. Semaphore in unknown state.
Status OsSemaphoreDelete( OsSemaphoreType& rSemaphore );

/// \brief Takes (decrements, P) a semaphore. Not ISR safe.
///
/// Decrements the semahore in thread safe manner, or blocks until value of
/// semaphore is greater than zero and then decrements it.
/// \param[out] rSemaphore      Semaphore to be taken.
/// \retval     Status::OK    Took semaphore successfully.
/// \retval     Status::ERROR Error. Semaphore is in unknown state.
Status OsSemaphoreBlockingTake( OsSemaphoreType& rSemaphore );

/// \brief Takes (decrements, P) a semaphore. Not ISR safe.
///
/// Decrements the semahore in thread safe manner. Task calling this function
/// doesn't block, but function returns an error denoting that it would block.
/// \param[out] rSemaphore      Semaphore to be taken.
/// \retval     Status::OK    Took semaphore successfully.
/// \retval     Status::WOULDBLOCK Can't take semaphore at this time.
/// \retval     Status::ERROR Error. Semaphore is in unknown state.
Status OsSemaphoreNonBlockingTake( OsSemaphoreType& rSemaphore );

/// \brief Gives (increments, V) a semaphore. Not ISR safe.
///
/// Incremnts the semahore in thread safe manner. Doesn't block.
/// \param[out] rSemaphore      Semaphore to be given.
/// \retval     Status::OK    Gave semaphore successfully.
/// \retval     Status::ERROR Error. Semaphore is in unknown state.
Status OsSemaphoreNonBlockingGive( OsSemaphoreType& rSemaphore );

///@}

/// \name OsCriticalSection
///@{

/// \brief Enters a critical section.
///
/// Ensures that part of code that is enclosed by critical section will be
/// executed without interruptions.
/// \retval     Status::OK    Entered critical section successfully.
/// \retval     Status::ERROR Error. System in unknown state.
Status OsCriticalSectionEnter();

/// \brief Exits a critical section.
///
/// Ensures that part of code that is enclosed by critical section will be
/// executed without interruptions.
/// \retval     Status::OK    Exited critical section successfully.
/// \retval     Status::ERROR Error. System in unknown state.
Status OsCriticalSectionExit();

///@}

/// \name OsQueue
///@{

/// \brief Creates a queue.
///
/// Creates a queue, of OS specific type, with a buffer for a given number of
/// elements of a given size each.
/// \param[out] rQueue          Queue to be created.
/// \param[in]  queueLength     Number of elements in the queue.
/// \param[in]  elementSize     Size of each elements in the queue.
/// \retval     Status::OK    Created queue successfully.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueCreate( OsQueueType& rQueue, const size_t queueLength,
                      const size_t elementSize );

/// \brief Deletes a queue.
///
/// Deletes a previously created queue. No task should be waiting on the queue
/// when it's deleted.
/// \param[in]  rQueue          Queue to be deleted.
/// \retval     Status::OK    Deleted queue successfully.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueDelete( OsQueueType& rQueue );

/// \brief Receives an item from a queue. Blocks if data is not available.
///
/// If data is not available in the queue, a task calling this function is
/// blocked until data becomes available. Can block indefinitely.
/// \param[out] rQueue          Queue to be created.
/// \param[in]  pItemBuffer     Pointer to buffer for received item.
/// \retval     Status::OK    Received data successfully.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueReceiveBlocking( OsQueueType& rQueue, void* const pItemBuffer );

/// \brief Sends an item to a queue. Blocks if queue is full.
///
/// If queue is full, a task calling this function is blocked until buffer space
/// in the queue becomes available. Can block indefinitely.
/// \param[out] rQueue          Queue to be created.
/// \param[in]  pItemToSend     Pointer to item to be sent.
/// \retval     Status::OK    Received data successfully.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueSendBlocking( OsQueueType& rQueue,
                            const void* const pItemToSend );

/// \brief Receives an item from a queue. Doesn't block if data is not
/// available.
///
/// If data is not available in the queue, a task calling this function isn't
/// blocked, but the function returns error value denoting that it would block.
/// Should be called again when data becomes available.
/// \param[out] rQueue          Queue to be created.
/// \param[in]  pItemBuffer     Pointer to buffer for received item.
/// \retval     Status::OK            Received data successfully.
/// \retval     Status::WOULDBLOCK    No data available to receive.
/// \retval     Status::ERROR         Error. Queue in unknown state.
Status OsQueueReceiveNonBlocking( OsQueueType& rQueue,
                                  void* const pItemBuffer );

/// \brief Sends an item to a queue. Doesn't block if queue is full.
///
/// If queue is full, a task calling this function isn't blocked , but the
/// function returns error value denoting that it would block. Should be called
/// again when buffer space in the queue becomes avaiable.
/// \param[out] rQueue          Queue to be created.
/// \param[in]  pItemToSend     Pointer to item to be sent.
/// \retval     Status::OK    Received data successfully.
/// \retval     Status::WOULDBLOCK    No data available to receive.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueSendNonBlocking( OsQueueType& rQueue,
                               const void* const pItemToSend );

/// \brief Checks if queue is empty.
///
/// Checks in threadsafe manner if the queue is empty.
/// \param[out] rQueue          Queue to check.
/// \retval     Status::TRUE  Queue is empty.
/// \retval     Status::FALSE Queue is not empty.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueIsEmpty( const OsQueueType& rQueue );

/// \brief Checks if queue if full.
///
/// Checks in threadsafe manner if the queue is full.
/// \param[out] rQueue          Queue to check.
/// \retval     Status::TRUE  Queue is full.
/// \retval     Status::FALSE Queue is not full.
/// \retval     Status::ERROR Error. Queue in unknown state.
Status OsQueueIsFull( const OsQueueType& rQueue );

///@}
}

#endif // OSINTERPROCESS_HPP
